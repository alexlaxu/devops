# Use the official Nginx image as the base
FROM nginx:alpine

# Copy the static content (index.html) into the nginx default directory
COPY ./index.html /usr/share/nginx/html/index.html

# Expose port 80 for the web server
EXPOSE 80

# Start Nginx and keep it running in the foreground
CMD ["nginx", "-g", "daemon off;"]